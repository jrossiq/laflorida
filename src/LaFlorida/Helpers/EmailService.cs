﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net.Mail;
using System.Threading.Tasks;
using System.Web;

namespace LaFlorida.Helpers
{
    public class EmailService
    {
        public static Task SendAsync(string subject, string body)
        {
            // Configure the client:
            var client = new SmtpClient(ConfigurationManager.AppSettings["SMTPServer"]);

            client.Port = Convert.ToInt32(ConfigurationManager.AppSettings["SMTPPort"]);
            client.DeliveryMethod = System.Net.Mail.SmtpDeliveryMethod.Network;
            client.UseDefaultCredentials = false;

            // Creatte the credentials:
            System.Net.NetworkCredential credentials = new System.Net.NetworkCredential(ConfigurationManager.AppSettings["SMTPUserName"], ConfigurationManager.AppSettings["SMTPPassword"]);
            client.EnableSsl = ConfigurationManager.AppSettings["SMTPSslEnabled"].ToString().ToLower() == "true";
            client.Credentials = credentials;

            // Create the message:
            var sendTo = ConfigurationManager.AppSettings["EmailTo"];
            var mail = new System.Net.Mail.MailMessage(ConfigurationManager.AppSettings["EmailFrom"], sendTo);
            mail.To.Add("jrossiq@gmail.com");
            mail.Subject = subject;
            mail.Body = body;
            mail.IsBodyHtml = true;
            

            return client.SendMailAsync(mail);
        }
    }

}