app.controller('homeController', function ($scope, $http) {
    var vm = this;

    vm.images = [
        '3.jpg',
        '5.jpg',
        '6.jpg',
        '7.jpg',
        '12.jpg',
        '13.jpg',
        '18.jpg',
        '25.jpg',
        '26.jpg',
        '28.jpg',
        '29.jpg',
    ];

    

    vm.showImagePopup = function () {
        vm.index = 0;
    }

    vm.message = {};
    vm.messageFormTriggered = false;
    vm.formSent = false;

    vm.sendMessage = function() {
        debugger;
        if (vm.message.name && vm.message.phone && vm.message.email && vm.message.body) {
            $http.post('/home/PostSendEmail', vm.message).then(function() {
                vm.messageFormTriggered = false;
                vm.formSent = true;
                vm.message = {};    
            }, function(response) {
                    console.log(response);
            });     
        }
        else {
            vm.messageFormTriggered = true;
            vm.formSent = false;
        }
    }

    vm.previous = function(){
        var max = vm.images.length;
        vm.index = vm.index == 0 ? max - 1 : vm.index - 1; 
    }

    vm.next = function() {
        var max = vm.images.length;
        vm.index = vm.index == max - 1 ? 0 : vm.index + 1;
    }

    
    $('#carousel123').carousel({ interval: false });

    $('#carousel123.carousel-showsixmoveone .item').each(function() {
        var itemToClone = $(this);
        for (var i = 1; i < 3; i++) {
            itemToClone = itemToClone.next();

            // wrap around if at end of item collection
            if (!itemToClone.length) {
                itemToClone = $(this).siblings(':first');
            }

            // grab item, clone, add marker class, add to collection
            itemToClone.children(':first-child').clone()
                .addClass("cloneditem-" + (i))
                .appendTo($(this));
        }
    });
        
    $('#carousel_mobile').carousel({ interval: false });

    $('#carousel_mobile.carousel-showsixmoveone .item').each(function () {
        var itemToClone = $(this);
        for (var i = 1; i < 1; i++) {
            itemToClone = itemToClone.next();

            // wrap around if at end of item collection
            if (!itemToClone.length) {
                itemToClone = $(this).siblings(':first');
            }

            // grab item, clone, add marker class, add to collection
            itemToClone.children(':first-child').clone()
                .addClass("cloneditem-" + (i))
                .appendTo($(this));
        }
    });

    setTimeout(function() {
        $('#galeryPopup').popup({
            color: 'black',
            opacity: 1,
            transition: '0.3s',
            scrolllock: true
        });
        $.fn.popup.defaults.pagecontainer = '#page'
    }, 3000)

    
})