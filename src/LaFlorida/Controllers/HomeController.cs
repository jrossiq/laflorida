﻿using LaFlorida.Helpers;
using LaFlorida.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace LaFlorida.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }

        [HttpPost]
        public async Task<ActionResult> PostSendEmail(EmailMessage message)
        {
            try
            {
                var body = new StringBuilder();
                body.AppendLine($"<strong>Nombre:</strong> {message.Name} <br/>");
                body.AppendLine($"<strong>Teléfono:</strong> {message.Phone}<br/>");
                body.AppendLine($"<strong>Email:</strong> {message.Email}<br/><br/>");
                body.AppendLine($"<strong>Mensaje:</strong> {message.Body}");                

                await EmailService.SendAsync("Lo Florida: Consulta sobre lote", body.ToString());
                return new JsonResult();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}